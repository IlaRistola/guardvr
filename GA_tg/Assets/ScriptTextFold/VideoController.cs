﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoController : MonoBehaviour
{
    public GameObject vidCont;
    public VideoClip[] vids; //12: 0 2 4 7 10 12 & 1 3 6 8 11 13
                             //toinen playeri bystanderille?
    public VideoPlayer vp;
    public VideoPlayer vp2;
    public GameObject tabs;
    public MeshRenderer vidMes1;
    public MeshRenderer vidMes2;
    //public Animation anim;
    public GameObject MeshTrigger;
    
    public int vidNum = 0;
    public bool entry = true;
    public bool iLoop = true;
    public bool cont = false;   //entryn tila vaihtelee, tartten tän vähentää DBcalculaattorin kuormitusta ja laittaa sen "pauselle"
    bool done = false;

    void Start()
    {
        vidMes1.enabled = false;
        vp2.isLooping = true;
        vp.loopPointReached += videoEnding;
        vp2.loopPointReached += videoLoop;
        entry = true;
    }
    
    public void PlayVideo(int num)
    {
        if (entry)
        {
            vp.clip = vids[num];
            vp.Prepare();
            vidNum = num;
            iLoop = false;
            entry = false;
            
        } 
    }

    void videoEnding(VideoPlayer vp)
    {
        vp2.Play();
        iLoop = true;
        vidMes2.enabled = true;
        vidMes1.enabled = false;
        vp.Pause();
        entry = true;
        cont = true;
    }

   void videoLoop(VideoPlayer vp2)
    {
        if (!iLoop && vp.isPrepared)
        {
            vp.Play();
            vidMes1.enabled = true;
            vidMes2.enabled = false;
            vp2.Pause();
            vp2.clip = vids[vidNum + 1];
            vp2.Prepare();
            //AnimPlayer();
        }
    }
    /*void AnimPlayer()
    {
        if (vidNum == 6 && !done)
        {
            MeshTrigger.gameObject.SetActive(false);
            //anim.Play("stomp");
            done = true;
        }
            
        else if(vidNum == 10)
        {
            MeshTrigger.gameObject.SetActive(false);
            //anim.Play("angry_stomp");
            done = true;
        }
    }*/

}
