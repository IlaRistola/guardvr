﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoInteract : MonoBehaviour
{
    private GameObject thisOne;
    public TabletUpdateScript tabs;
    private int lineOneCrossed = 0;
    private int lineTwoCrossed = 0;

    public Canvas canvas;
    public VideoController videoController;
    public bool collided = false;

    private void OnTriggerEnter(Collider other)
    {
        thisOne = this.gameObject;
        if (other.gameObject.tag == "Player")
        {

            //first time player is rossing the closer line, is in correct distance
            if (thisOne.gameObject.CompareTag("closerDistance") && lineOneCrossed == 0)
            {
                lineOneCrossed++;
                videoController.PlayVideo(2);
                //videoController.PlayLoop(0);
                canvas.gameObject.SetActive(true);
                collided = true;
                tabs.NewText("Oikea etäisyys", true);
                tabs.ImageController(3);
                
            }

            //ei muuta videota, lähettää vaan viestin
            //eli linecrossedii ei enää trvitse muuten
            //onko wait kans turha? jos ei kerran triggeröidy etäisyydestä
            else if (thisOne.gameObject.CompareTag("closerDistance") && lineOneCrossed%2 == 1)
            {
                tabs.NewText("Vartija ei peruuta! MUUTA", true);
                tabs.ImageController(3); //muuta
                //Wait();
            }

            //first time player is crossing the line
            else if (thisOne.gameObject.CompareTag("fartherDistance") && lineTwoCrossed % 2 == 0)
            //if crossed, too close!!
            {
                lineTwoCrossed++;
                
                tabs.NewText("Sinun tulee pitää noin kolmen metrin etäisyys kohteeseen, aggressiivinen henkilö voi käyttäytyä arvaamattomasti", true);
                //tähän pitää lisää se punanen mesh
                tabs.ImageController(3);//MUUTA OIKEAKSI
                //Wait();
            }

            //player has crossed the further line again, so is closer to startpoint. Char back
            else if (thisOne.gameObject.CompareTag("fartherDistance"))
            {
                tabs.NewText("Oikea etäisyys", true);
                tabs.ImageController(5); //0=none
                //Wait();
            }
        }
    }
    IEnumerator WaitT()
    {
        yield return new WaitForSecondsRealtime(2);
    }
}