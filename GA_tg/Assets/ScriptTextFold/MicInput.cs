﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicInput : MonoBehaviour {

	public string selectedDevice;

    void Start() {
		selectedDevice = Microphone.devices[0].ToString();
		AudioSource aud = GetComponent<AudioSource>();
		//aud.clip = Microphone.Start("Built-in Microphone", true, 1, 48000); //built-in mic??
		aud.clip = Microphone.Start(selectedDevice, true, 1, 44100);  //44100?
		aud.loop = true;
		while (!(Microphone.GetPosition (null) > 0)) {
		}
		aud.Play();
	}
}
