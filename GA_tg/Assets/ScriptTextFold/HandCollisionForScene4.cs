﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandCollisionForScene4 : MonoBehaviour
{
    public ButtonForScene4 ready;

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.tag == "rightHand" && this.gameObject.tag == "grabWrist"))
        {
            ready.ReadyRight(true);
            Debug.Log("käsi");
        }
        else if ((other.gameObject.tag == "leftHand" && this.gameObject.tag == "grabElbow"))
            ready.ReadyLeft(true);
        else if (this.gameObject.tag == "seinä" && other.gameObject.tag == "Player")
        {
            Debug.Log("osuma");
            this.GetComponent<MeshRenderer>().enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if ((other.gameObject.tag == "rightHand" && this.gameObject.tag == "grabWrist"))
            ready.ReadyLeft(false);
        else if ((other.gameObject.tag == "leftHand" && this.gameObject.tag == "grabElbow"))
            ready.ReadyRight(false);
        else if (this.gameObject.tag == "seinä" && other.gameObject.tag == "Player")
        {
            this.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}