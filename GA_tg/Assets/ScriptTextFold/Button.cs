﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Button : MonoBehaviour
{
    public GameObject quad;
    public Canvas cancan;
    public TabletUpdateScript tabs;
    public VideoController vc;
    bool pressed = false;
    public GameObject Lines;

    public void Awake()
    {
        tabs = GameObject.FindGameObjectWithTag("tablet").GetComponent<TabletUpdateScript>();
    }

    //for testing
    void Update()
    {
        if (Input.GetKeyDown("space") && pressed == false)
        {
            StartProgram();
            pressed = true;
        }
        else if (Input.GetKeyDown("space") && pressed == true){
            ChangeScene();
        }
    }

    public void StartProgram()
    {
        quad.gameObject.SetActive(true);
        cancan.gameObject.SetActive(false);
        tabs.NewText("Liiku eteenpäin hyvälle etäisyydelle kohteesta", true);
        tabs.ImageController(5);
        vc.PlayVideo(0);
        Lines.gameObject.SetActive(false);
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(2);
    }
}
