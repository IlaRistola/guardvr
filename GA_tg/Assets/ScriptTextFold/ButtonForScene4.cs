﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonForScene4 : MonoBehaviour
{
    public Image canvasimg;
    public UnityEngine.UI.Button button;
    public GameObject Lines;
    public Text txt;
    private int count = 0;
    public Canvas canvas;
    public Text readytext;
    public GameObject tablet;
    public GameObject Spede;
    public TabletUpdateScript tabs;

    private bool right = false;
    private bool left = false;
    public bool ready = false;

    void Update()
    {
        if(Time.frameCount%2 == 0)
        {
            if (Input.GetKeyDown("space") && count == 0)
            {
                Debug.Log("space painettu");
                StartScene4();
                count++;

            }
            //else if (Input.GetKeyDown("space"))
            //    RestartProgram();

            Readyy();
        }
    }
    
    void Awake()
    {
        tablet = GameObject.FindWithTag("tablet");
        tabs = GameObject.FindWithTag("tablet").gameObject.GetComponent<TabletUpdateScript>();
        tablet.gameObject.SetActive(false);
        
    }

    public void ReadyRight(bool righ)
    {
        right = righ;
    }

    public void ReadyLeft(bool lef)
    {
        left = lef;
    }

    public void Readyy()
    {
        if (right && left)
        {
            tablet.gameObject.SetActive(true);
            tabs.NewText("Käänny ympäri!", true);
            txt.gameObject.SetActive(true);
            Lines.gameObject.SetActive(true);
            Spede.gameObject.SetActive(false);
            Vector3 v = new Vector3(-1.65f, -1.26f, 3.02f);
            canvas.gameObject.transform.position = v;
            canvas.gameObject.transform.rotation = Quaternion.identity;
            txt.text = "Demo suoritettu! Kävele takaisin vihreään ympyrään palaaksesi alkuun"+ "" +
                "(ja käänny uudestaan ympäri)";
            right = false;
            left = false;
            ready = true;
        }
    }
    
    public bool IsReady()
    {
        return ready;
    }

    public void StartScene4()
    {
        Lines.gameObject.SetActive(false);
        button.gameObject.SetActive(false);
        canvasimg.gameObject.SetActive(false);
        txt.gameObject.SetActive(false);
    }
    
}
