﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceCheck : MonoBehaviour
{
    public TabletUpdateScript tabs;
    public Text txt;
    public UnityEngine.UI.Button btn;
    public UnityEngine.UI.Button csbtn;
    public Canvas cancan;
    public Image controllerimage;
    public GameObject VoiceController;
    public GameObject Lines;
    public bool done = false;

    public MeshTrigger vidI;

    public void Awake()
    {
        tabs = GameObject.FindGameObjectWithTag("tablet").GetComponent<TabletUpdateScript>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !done)
        {
            txt.text = "Tehtäväsi on rauhoittaa tilanne viraston tiloissa. " +
            "" +
            "" +
            "Liiku noin kolmen metrin etäisyydelle kohteesta. Kädessäsi olevasta tabletista saat ohjeita, joista saat ilmoituksen värinällä vasempaan ohjaimeen. " +
            "" +"Puhu selkeällä äänellä mikin kuvan ollessa päällä. " +
            "" +
            "Aloita painamalla oikealla ohjaimella 'Aloita' - nappia.";
            btn.gameObject.SetActive(true);
            controllerimage.gameObject.SetActive(true);

            tabs.NewText("Heippa!", true);
            tabs.ImageController(1); //infokuva

            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            Invoke("Check", 120);
            done = true;
        }

    }

    void Check()
    {
        if (tabs.text.ToString() == "Heippa!")
        {
            tabs.NewText("Liiku lähemmäs kohdetta", true);
            tabs.ImageController(4); //liian kaukana, kuva 5
        }
    }

    public void SceneChange()
    {
        Debug.Log("press space");
        VoiceController.SetActive(false);
        cancan.gameObject.SetActive(true);
        controllerimage.gameObject.SetActive(false);
        btn.gameObject.SetActive(false);
        csbtn.gameObject.SetActive(true);
        Lines.gameObject.SetActive(true);
        txt.text = "Kohde on yhä aggressiivinen. "+
            "" +
            "Sinun tulee ottaa kohde kiinni saadaksesi hänet poistumaan tilasta. " +
            "" +
            "Paina nappia jatkaaksesi.";
        
    }
    
}

