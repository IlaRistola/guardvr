﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject main;
    // Start is called before the first frame update
    void Start()
    {
        main = GameObject.Find("CenterEyeAnchor");
        this.gameObject.GetComponent<Canvas>().worldCamera = main.GetComponent<Camera>();
    }
}
