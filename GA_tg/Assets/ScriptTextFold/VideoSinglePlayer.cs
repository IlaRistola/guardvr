﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoSinglePlayer : MonoBehaviour
{
    public GameObject vidCont;
    public VideoClip[] vids; //12: 0 2 4 7 10 12 & 1 3 6 8 11 13
                             //toinen playeri bystanderille?
    public VideoPlayer vp;
    public GameObject tabs;
    int vidNum = 0;
    public bool entry = true;
    public bool iLoop = true;
    public bool cont = false;   //entryn tila vaihtelee, tartten tän vähentää DBcalculaattorin kuormitusta ja laittaa sen "pauselle"


    void Start()
    {
        vp.isLooping = true;
        vp.loopPointReached += videoEnding;
        entry = true;
    }

    private void Update()
    {
        /*if (vp.isPlaying)
        {
            vp
        }*/
    }

    //private void Update()   //tän updaten voi heittää nevadaan, on pelkästään sen takia, että huomaa vaihtuuko videot.
    //{
    //    Debug.Log(vp.clip);
    //    Debug.Log(vp2.clip);
    //}

    public void PlayVideo(int num)
    {
        if (entry)
        {
            Debug.Log("jatatata");
            vp.clip = vids[num];
            vidNum = num;
            iLoop = false;
            entry = false;
            vp.isLooping = false;
        }
    }

    void videoEnding(VideoPlayer vp)
    {
        if (!iLoop)
        {
            vp.clip = vids[vidNum];
            iLoop = true;

        }
        else if (iLoop)
        {
            vp.clip = vids[vidNum + 1];
            //vp.Play();
            //iLoop = true;
            entry = true;
            cont = false;
            vp.isLooping = true;
        }
    }
}

/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoC : MonoBehaviour
{
    public GameObject vidCont;
    public VideoClip[] vids; //12: 0 2 4 7 10 12 & 1 3 6 8 11 13
                             //toinen playeri bystanderille?
    public VideoPlayer vp;
    public VideoPlayer vp2;
    public GameObject tabs;
    public MeshRenderer vidMes1;
    public MeshRenderer vidMes2;
    int vidNum = 0;
    public bool entry = true;
    public bool iLoop = true;
    public bool cont = false;   //entryn tila vaihtelee, tartten tän vähentää DBcalculaattorin kuormitusta ja laittaa sen "pauselle"


    void Start()
    {
        vidMes1.enabled = false;
        vp2.isLooping = true;
        vp.loopPointReached += videoEnding;
        vp2.loopPointReached += videoLoop;
        entry = true;
    }

    //private void Update()   //tän updaten voi heittää nevadaan, on pelkästään sen takia, että huomaa vaihtuuko videot.
    //{
    //    Debug.Log(vp.clip);
    //    Debug.Log(vp2.clip);
    //}

    public void PlayVideo(int num)
    {
        if (entry)
        {
            Debug.Log("jatatata");
            vp.clip = vids[num];
            vp.Prepare();
            vidNum = num;
            iLoop = false;
            entry = false;
        }
    }

    void videoEnding(VideoPlayer vp)
    {
        vp2.Play();
        iLoop = true;
        vidMes2.enabled = true;
        vidMes1.enabled = false;
        vp.Pause();
        entry = true;
        cont = false;
    }

    void videoLoop(VideoPlayer vp2)
    {
        if (!iLoop && vp.isPrepared)
        {
            vp.Play();
            vidMes1.enabled = true;
            vidMes2.enabled = false;
            vp2.Pause();
            vp2.clip = vids[vidNum + 1];
            vp2.Prepare();
        }
    }
}

    */