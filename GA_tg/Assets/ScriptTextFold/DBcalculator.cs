﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DBcalculator : MonoBehaviour
{
    public float RmsValue;
    public float DbValue;
    public float PitchValue;
    public Slider slid;
    public TabletUpdateScript tabs;
    public Text debugText;
    public PlaceCheck pc;
    public Image micpic;
    //ei ole eri scenejä mutta pitää kirjaa siitä mitä videoita pistetään eteenpäin
    private int sceneNumber = 1; //1,2,3,4 = good endings  1,5,6 = bad endings

    private int counter = 0;
    public VideoController vc;

    private const int QSamples = 1024;
    private const float RefValue = 0.1f;
    private const float Threshold = 0.02f;

    float[] _samples;
    private float[] _spectrum;
    private float _fSample;
    public float slidValue = 0;
    private bool start = true;
    private bool badstart = true;
    public float lowRange = 0.20f;
    public float highRange = 0.80f;
    public int AssessingTime = 4;
    public bool success = false;
    private bool hiljaisuus = false;


    public void Awake()
    {

        _samples = new float[QSamples];
        _spectrum = new float[QSamples];
        _fSample = AudioSettings.outputSampleRate;
        tabs = GameObject.FindGameObjectWithTag("tablet").GetComponent<TabletUpdateScript>();
        pc = GameObject.FindGameObjectWithTag("startteri").GetComponent<PlaceCheck>();
        micpic.gameObject.GetComponent<Image>().enabled = false;
    }
    public void SliderAction()
    {
        
        slid.value = (float)System.Math.Round(slidValue, 2);
    }

    void Update()
    {
        if (vc.cont && !success)
        {
            //counter nollataan successin yhteydessä
            if (counter == 0)
            {
                micpic.gameObject.GetComponent<Image>().enabled = true;
                hiljaisuus = true;

                if (sceneNumber == 1)
                {

                    tabs.NewText("Ilmoita asemasi ja tarkkaile näytön liukuria oikean äänenvoimakkuuden löytämiseksi", true);
                    tabs.ImageController(5);
                    StartCoroutine("Wait");//pidetään tekstiä 10 s näkyvissä
                    counter++;
                    AssessingTime = 3;
                }
                else if (sceneNumber == 2)
                {
                    tabs.NewText("Ilmoita toimivaltasi ja pyydä kohdetta poistumaan", true);
                    tabs.ImageController(5);
                    StartCoroutine("Wait");//pidetään tekstiä 10 s näkyvissä
                    counter++;
                }
                else if (sceneNumber == 3 || sceneNumber == 6)
                {
                    tabs.NewText("Ilmoita käyttäväsi tarvittaessa voimakeinoja", true);
                    tabs.ImageController(5);
                    StartCoroutine("Wait");//pidetään tekstiä 10 s näkyvissä
                    AssessingTime = 4;
                    lowRange = 0.3f;
                    highRange = .9f;
                    counter++;
                }
                else if (sceneNumber == 5)
                {
                    tabs.NewText("Ilmoita toimivaltasi ja pyydä kohdetta poistumaan RAUHALLISESTI", true);
                    tabs.ImageController(5);
                    StartCoroutine("Wait");//pidetään tekstiä 10 s näkyvissä
                    counter++;
                }
                else if (sceneNumber == 4)//to scene 4
                {
                    tabs.NewText("Ilmoita siirtyväsi voimakeinojen käyttöön", true);
                    tabs.ImageController(5);
                    counter++;
                    AssessingTime = 2;
                }
                //kutonen on sama kuin kolmone anyway... mitä järkeä kirjottaa uudestaa
            }
            AnalyzeSound();

            if ((DbValue / 100f) >= slidValue)
            {
                slidValue = (DbValue / 100.0f);
            }
            else if (slidValue > 0)
            {
                slidValue = slidValue - 0.005f;
            }
            else if (slidValue <= 0)
            {
                slidValue = 0;
            }
            
                SliderAction();
            if (slidValue == 0f && hiljaisuus == false)
            {
                tabs.NewText("Puhu kovempaa", false);
                tabs.ImageController(5);
                StopAllCoroutines();
                Debug.Log("All coroutines stopped");
                start = true;
                badstart = true;
            }
            else if (slidValue > lowRange && slidValue < highRange)
            {
                if (start)
                {
                    StartCoroutine("NextScene");
                    Debug.Log("Good volume started");
                    start = false;
                }
                else if (badstart)
                {
                    StopCoroutine("TalkingTooLoud");
                    badstart = true;
                }
            }
            else if (slidValue > highRange)
            {
                if (badstart)
                {
                    StartCoroutine("TalkingTooLoud");
                    Debug.Log("Loud started");
                    badstart = false;
                }
            }
        }
        else if (success == true)
        {
            switch (sceneNumber){
                case 1:
                    if (vc.entry == true) {
                        success = false;
                        StopAllCoroutines();
                        micpic.gameObject.GetComponent<Image>().enabled = false;
                        slidValue = 0f;
                        slid.value = 0f;
                        Debug.Log(sceneNumber);
                        counter = 0;
                        tabs.NewText("Jatka tilanteen seuraamista", true);
                        vc.cont = false;
                        vc.PlayVideo(2);
                        sceneNumber++;
                        break;
                    }
                    break;

                case 2: 
                    if (vc.entry == true) {
                        success = false;

                        StopAllCoroutines();
                        tabs.NewText("Kohde ei ole rauhoittunut, jatka tilanteen seuraamista", true);
                        micpic.gameObject.GetComponent<Image>().enabled = false;
                        slidValue = 0;
                        slid.value = 0;
                        Debug.Log(sceneNumber);
                        counter = 0;
                        vc.cont = false;
                        vc.PlayVideo(4);
                        sceneNumber++;
                    }
                    break;
                case 3://toinen käsky
                    if (vc.entry == true)
                    {
                        success = false;
                        StopAllCoroutines();
                        micpic.gameObject.GetComponent<Image>().enabled = false;
                        slidValue = 0;
                        slid.value = 0;
                        Debug.Log(sceneNumber);
                        counter = 0;
                        tabs.NewText("Kohde on aggressiivinen", true);
                        vc.cont = false;
                        vc.PlayVideo(6);
                        sceneNumber++;
                    }
                    break;
                case 4://toinen käsky, kohti skeneä 4!
                    micpic.gameObject.GetComponent<Image>().enabled = false;
                    slidValue = 0;
                    slid.value = 0;
                    tabs.NewText("Jatka seuraavaan osaan", true);
                    Debug.Log(sceneNumber);
                    pc.SceneChange();
                    break;

                case 5://bad ending, recoverable

                    if (vc.entry == true)
                    {
                        success = false;
                        StopAllCoroutines();
                        micpic.gameObject.GetComponent<Image>().enabled = false;
                        slidValue = 0;
                        slid.value = 0;
                        Debug.Log(sceneNumber);
                        counter = 0;
                        tabs.NewText("Puhuit liian kovaa, kohde on aggressiivinen", true);
                        vc.cont = false;
                        vc.PlayVideo(8);
                        sceneNumber++;
                    }
                    break;
                case 6://bad ending, to scene 4

                    if (vc.entry == true)
                    {
                        success = false;
                        StopAllCoroutines();
                        micpic.gameObject.GetComponent<Image>().enabled = false;
                        slidValue = 0;
                        slid.value = 0;
                        Debug.Log(sceneNumber);
                        counter = 0;
                        tabs.NewText("Liian korkea äänensävy johtaa arvaamattomiin tilanteisiin", true);
                        vc.cont = false;
                        vc.PlayVideo(10);
                        sceneNumber = 4;
                    }
                    break;

                default:
                    Debug.Log("Default triggered");
                    Debug.Log(sceneNumber);
                    break;

            }
        }  
    }
    //waits for 4 seconds, after that its succesful
    IEnumerator NextScene()
    {
        badstart = true;
        yield return new WaitForSeconds(AssessingTime);
        
        success = true;
        

    }
    IEnumerator TalkingTooLoud()
    {
        start = true;
        yield return new WaitForSeconds(AssessingTime - 1);
        if (sceneNumber == 1)
            sceneNumber = 2;
        else if (sceneNumber == 2)
            sceneNumber = 5;
        else if (sceneNumber == 3)
            sceneNumber = 6;
        else sceneNumber = 4;
        
        success = true;


    }
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(4);
        hiljaisuus = false;
    }

    void AnalyzeSound()
    {
        GetComponent<AudioSource>().GetOutputData(_samples, 0); // fill array with samples
        int i;
        float sum = 0;
        for (i = 0; i < QSamples; i++)
        {
            sum += _samples[i] * _samples[i]; // sum squared samples
        }
        RmsValue = Mathf.Sqrt(sum / QSamples); // rms = square root of average
        DbValue = 200 * Mathf.Log10(RmsValue / RefValue); // calculate dB
        if (DbValue < 0) DbValue = 0; // clamp it to -160dB min
                                      // get sound spectrum
        GetComponent<AudioSource>().GetSpectrumData(_spectrum, 0, FFTWindow.BlackmanHarris);
        float maxV = 0;
        var maxN = 0;
        for (i = 0; i < QSamples; i++)
        { // find max 
            if (!(_spectrum[i] > maxV) || !(_spectrum[i] > Threshold))
                continue;

            maxV = _spectrum[i];
            maxN = i; // maxN is the index of max
        }
        float freqN = maxN; // pass the index to a float variable
        if (maxN > 0 && maxN < QSamples - 1)
        { // interpolate index using neighbours
            var dL = _spectrum[maxN - 1] / _spectrum[maxN];
            var dR = _spectrum[maxN + 1] / _spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        PitchValue = freqN * (_fSample / 2) / QSamples; // convert index to frequency

    }
}
