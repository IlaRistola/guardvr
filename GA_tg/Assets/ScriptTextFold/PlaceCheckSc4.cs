﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlaceCheckSc4 : MonoBehaviour
{
    public ButtonForScene4 btn;

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && btn.IsReady())
        {
            SceneManager.LoadScene(1);
            
        }

    }
}
