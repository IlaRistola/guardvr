﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshTrigger : MonoBehaviour
{
    public GameObject Player;
    public TabletUpdateScript tabs;
    public GameObject child;
    //public GameObject voiceController;
    
    private bool lineOneCrossed = false;
    private bool lineTwoCrossed = true;
    private bool lineOneCrossedFirstTime = true;

    public Canvas canvas;
    public VideoController videoController;
    public bool collided = false;
    public bool ekaLooppi = false;

    public void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        tabs = GameObject.FindGameObjectWithTag("tablet").GetComponent<TabletUpdateScript>();
    }

    public void Update()
    {
        //Debug.Log(Player.GetComponent<Transform>().position.z - this.gameObject.transform.position.z);
        //kutsuu joka kolmannella framella
        if (Time.frameCount % 3 == 0)
        {
            

            //ei muuta videota, lähettää vaan viestin
            if (Player.GetComponent<Transform>().position.z - this.GetComponent<Transform>().position.z >= 4 && lineOneCrossed == true)
            {
                tabs.NewText("Vartijan ei tule peruuttaa kohdetta karkuun", true);
                tabs.ImageController(3);
                ekaLooppi = true;
                return;
            }//player has crossed the further line again, so is closer to startpoint. Char back
            //PERUUTTAA
            else if ((Player.GetComponent<Transform>().position.z - this.GetComponent<Transform>().position.z >= 3) && Player.GetComponent<Transform>().position.z - this.GetComponent<Transform>().position.z <= 4
                && (lineTwoCrossed == true || ekaLooppi == true ))
            {
                ekaLooppi = false;
                lineTwoCrossed = false;
                tabs.NewText("Oikea etäisyys", true);
                tabs.ImageController(5);
                this.GetComponent<MeshRenderer>().enabled = false;
                child.GetComponent<MeshRenderer>().enabled = false;
                return;
            }
            else if (Player.GetComponent<Transform>().position.z - this.GetComponent<Transform>().position.z <= 2.3)
            {
                this.GetComponent<MeshRenderer>().enabled = true;
                child.GetComponent<MeshRenderer>().enabled = true;
                tabs.NewText("Olet liian lähellä kohdetta!", true);
                return;
            }
            else if (Player.GetComponent<Transform>().position.z - this.GetComponent<Transform>().position.z <= 3 && lineTwoCrossed == false)
            //if crossed, too close!!
            {
                lineTwoCrossed = true;
                ekaLooppi = true;
                tabs.NewText("Pidä noin kolmen metrin etäisyys kohteeseen, aggressiivinen henkilö voi käyttäytyä arvaamattomasti", true);
                tabs.ImageController(3);
                return;
            }
            //first time player is rossing the closer line, is in correct distance
            //ETENEE
            else if (Player.GetComponent<Transform>().position.z - this.GetComponent<Transform>().position.z <= 4 && lineOneCrossed == false)
            {
                lineOneCrossed = true;
                ekaLooppi = true;
                tabs.NewText("Oikea etäisyys", true);
                tabs.ImageController(3);
                if (lineOneCrossedFirstTime == true)
                {
                    //voiceController.gameObject.SetActive(true);
                    canvas.gameObject.SetActive(true);
                    collided = true;
                    lineOneCrossedFirstTime = false;
                }
                return;
            }
        }
    }
    
}
