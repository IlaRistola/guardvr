﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabletUpdateScript : MonoBehaviour
{
    public Text text;
    public Text debugtext;
    public Sprite[] images;
    public Image TabletImage;
    
    //ei updatee, tekee kaiken newTextin kautta!!

    void Clear()
    {
        text.text = " ";
    }

    void Happen()
    {
        ControllerHaptic haptics = GetComponentInParent<ControllerHaptic>();
        if (text.text == " ")
        {
            if (haptics)
            {
                haptics.HapticEvent();
            }
        }
    }

    public void NewText(string newText, bool haps)
    {
        Clear();
        if (haps == true)
        {
            Happen();
        }
        text.text = newText;
    }

    public void ImageController(int imgnum)
    {
        //vaihdetaan kuvia 
        TabletImage.sprite = images[imgnum];
    }
}
