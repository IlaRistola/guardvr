﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class ControllerHaptic : MonoBehaviour
{
    public XRNode hand;

    public void HapticEvent()
    {
        // 1. 
        
        InputDevice device = InputDevices.GetDeviceAtXRNode(hand);
        

        //TODO muokkaa koodia niin että värisee kun tabletin viesti päivitetään
        // 2.
        HapticCapabilities capabilities;
        if (device.TryGetHapticCapabilities(out capabilities))
        {
            if (capabilities.supportsImpulse)
            {
                uint channel = 0;
                float amplitude = 0.5f;
                float duration = 0.3f;
                // 3. 
                device.SendHapticImpulse(channel, amplitude, duration);
            }
        }
    }
}
