namespace Oculus.Platform.Samples.VrVoiceChat
{
	using UnityEngine;
	using System.Collections;
	using UnityEngine.UI;

	// Helper class to attach to the main camera that raycasts where the
	// user is looking to select/deselect Buttons.
	public class VREyeRaycaster : MonoBehaviour
	{
		[SerializeField] private UnityEngine.EventSystems.EventSystem m_eventSystem = null;
        [SerializeField] private LineRenderer m_LineRenderer = null; // For supporting Laser Pointer
        public bool ShowLineRenderer = true;                         // Laser pointer visibility
        [SerializeField] private Transform m_TrackingSpace = null;   // Tracking space (for line renderer)

        private Button m_currentButton;
        private Camera camera;

        public bool ControllerIsConnected
        {
            get
            {
                OVRInput.Controller controller = OVRInput.GetConnectedControllers() & (OVRInput.Controller.LTrackedRemote | OVRInput.Controller.RTrackedRemote);
                return controller == OVRInput.Controller.LTrackedRemote || controller == OVRInput.Controller.RTrackedRemote;
            }
        }
        public OVRInput.Controller Controller
        {
            get
            {
                OVRInput.Controller controller = OVRInput.GetConnectedControllers();
                if ((controller & OVRInput.Controller.LTrackedRemote) == OVRInput.Controller.LTrackedRemote)
                {
                    return OVRInput.Controller.LTrackedRemote;
                }
                else if ((controller & OVRInput.Controller.RTrackedRemote) == OVRInput.Controller.RTrackedRemote)
                {
                    return OVRInput.Controller.RTrackedRemote;
                }
                return OVRInput.GetActiveController();
            }
        }

        private void EyeRayCast()
        {
           
        }

        void Update ()
		{
			RaycastHit hit;
			Button button = null;

			// do a forward raycast to see if we hit a Button
			if (Physics.Raycast(transform.position, transform.forward, out hit, 50f))
			{
				button = hit.collider.GetComponent<Button>();
			}

			if (button != null)
			{
				if (m_currentButton != button)
				{
					m_currentButton = button;
					m_currentButton.Select();
				}
			}
			else if (m_currentButton != null)
			{
				m_currentButton = null;
				if (m_eventSystem != null)
				{
					m_eventSystem.SetSelectedGameObject(null);
				}
			}
		}
	}
}
